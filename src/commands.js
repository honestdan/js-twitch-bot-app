let textCommands = require('../data/commands.json');
const {saveJsonToFile} = require('./util');

const commandMap = new Map([
    ['startraidsuggestions', startRaidSuggestions],
    ['stopraidsuggestions', stopRaidSuggestions],
    ['startraidvoting', startRaidVoting],
    ['stopraidvoting', stopRaidVoting]
]);

for (let key in textCommands) {
    if (textCommands.hasOwnProperty(key)) {
        if(!commandMap.has(key)){
            commandMap.set(key, () => { return textCommands[key]});
        }else{
            console.log(`Loading command error: ${key} already exists in our commandMap`);
        }
    }
}

function startRaidSuggestions(){

}

function stopRaidSuggestions(){

}

function startRaidVoting(){

}

function stopRaidVoting(){

}

function addNewCommand(newCommandName, newCommandMessage){
    if(!commandMap.has(newCommandName)){
        commandMap.set(newCommandName, () => newCommandMessage);
        textCommands[newCommandName] = newCommandMessage;
        saveJsonToFile(textCommands, 'commands');
        return `Adding new command: ${newCommandName}, will print out: ${newCommandMessage}.`;
    }else{
        return `The ${newCommandName} command already exists, please choose a unique command name`;
    }
}

function processCommand(message, messageMetaData) {
    var args = message.trim().substring(1).split(' ');
    var command = args.shift().toLowerCase();
    console.log("command: ", command, ", arguments: ", args);

    if (command === "add" && (messageMetaData.isStreamer || messageMetaData.isMod)) {
        if (args.length >= 2) {
            newCommandName = args.shift().toLowerCase();
            newCommandMessage = args.join(' ');
            feedbackMessage = addNewCommand(newCommandName, newCommandMessage);                     
        } else {
            feedbackMessage = "To use the add command, please follow this format: +add <nameOfCommand> <textToShow>"
        }
    } else {
        if (commandMap.has(command)) {
            feedbackMessage = commandMap.get(command)();
        } else {
            feedbackMessage = `There exists no command: ${command}.`;
        }
    }
    return feedbackMessage;
}

function isValidCommand(message, config) {

    if (!startsWithCommandSymbol(message, config)) {
        return false;
    }
    if (!firstWordIsAnExistingCommand(message, config)) {
        return false;
    }
    return true;
}

function startsWithCommandSymbol(message, config) {
    if (message.startsWith(config.commandSymbol)) {
        return true;
    }
    return false;
}

function firstWordIsAnExistingCommand(message, config) {
    return true;
    //parse the first word
    //look through our json commands to see if word matches property name
}

module.exports = {processCommand, isValidCommand, };