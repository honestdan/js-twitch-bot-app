//short hand for "when the dom is ready"
document.addEventListener("DOMContentLoaded", function() {

    //make connection
 var socket = io.connect('http://localhost:3000')

 //buttons and inputs
 const infoMessageElement = document.getElementById('info-message');
 const testButton = document.getElementById('test-button');


 //Emit message
 testButton.addEventListener('click', function(){
     socket.emit('test_event', {test_data : "Honest Dan is aight"})
 });

 //Listen on new_message
 socket.on("test_event", (data) => {
     infoMessageElement.innerHTML = `The data is <b> ${data.test_data}.`;
 })	
});


